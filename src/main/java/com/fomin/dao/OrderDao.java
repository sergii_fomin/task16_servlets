package com.fomin.dao;

import com.fomin.model.Order;

import java.util.SortedMap;

public interface OrderDao {
    void save(Order order);

    void update(long id, Order order);

    void delete(long id);

    SortedMap<Long, Order> getOrders();

    Order findOneById(long id);
}
