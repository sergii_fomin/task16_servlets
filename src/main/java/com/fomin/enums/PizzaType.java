package com.fomin.enums;

public enum PizzaType {
    Cheese, Vegan, Carbonara
}
