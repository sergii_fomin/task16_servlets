package com.fomin.service;

import com.fomin.model.Order;
import com.fomin.model.Pizza;
import com.fomin.payload.OrderRequest;

import java.util.SortedMap;

public interface OrderService {
    void save(Order order);

    void update(long id, OrderRequest orderRequest);

    void delete(long id);

    SortedMap<Long, Order> findAll();

    Pizza getPizzaImplementation(String pizza);
}
