package com.fomin.service.impl;

import com.fomin.dao.OrderDao;
import com.fomin.enums.PizzaType;
import com.fomin.model.Customer;
import com.fomin.model.Order;
import com.fomin.model.Pizza;
import com.fomin.model.impl.CarbonaraPizza;
import com.fomin.model.impl.CheesePizza;
import com.fomin.model.impl.VeganPizza;
import com.fomin.payload.OrderRequest;
import com.fomin.service.OrderService;

import java.util.SortedMap;

public class OrderServiceImpl implements OrderService {
    private OrderDao orderDao;

    public OrderServiceImpl(OrderDao orderDao) {
        this.orderDao = orderDao;
    }

    @Override
    public void save(Order order) {
        orderDao.save(order);
    }

    @Override
    public void update(long id, OrderRequest orderRequest) {
        Order order = orderDao.findOneById(id);
        order.setId(id);
        Order requestOrder = orderRequest.getOrder();
        Customer requestCustomer = requestOrder.getCustomer();
        String pizzaImplementation = orderRequest.getPizzaClass();
        Pizza pizza;
        if (pizzaImplementation != null) {
            pizza = getPizzaImplementation(pizzaImplementation);
            order.setOrderedPizza(pizza);
        }
        if (requestCustomer != null) {
            if (requestCustomer.getFirstName() != null) {
                order.getCustomer().setFirstName(requestCustomer.getFirstName());
            }
            if (requestCustomer.getLastName() != null) {
                order.getCustomer().setLastName(requestCustomer.getLastName());
            }
            if (requestCustomer.getPhoneNumber() != null) {
                order.getCustomer().setPhoneNumber(requestCustomer.getPhoneNumber());
            }
        }
        orderDao.update(id, order);
    }

    @Override
    public void delete(long id) {
        orderDao.delete(id);
    }

    @Override
    public SortedMap<Long, Order> findAll() {
        return orderDao.getOrders();
    }

    @Override
    public Pizza getPizzaImplementation(String orderedPizza) {
        Pizza pizza = null;
        if (orderedPizza.equalsIgnoreCase(PizzaType.Cheese.toString())) {
            pizza = new CheesePizza();
        } else if (orderedPizza.equalsIgnoreCase(PizzaType.Vegan.toString())) {
            pizza = new VeganPizza();
        } else if (orderedPizza.equalsIgnoreCase(PizzaType.Carbonara.toString())) {
            pizza = new CarbonaraPizza();
        }
        return pizza;
    }
}
