package com.fomin.model.impl;

import com.fomin.model.Pizza;

public class CarbonaraPizza implements Pizza {
    private final String name = "Carbonara";

    private double price = 333;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public double getPrice() {
        return price;
    }
}
