package com.fomin.model.impl;

import com.fomin.model.Pizza;

public class VeganPizza implements Pizza {
    private final String name = "Vegan pizza";

    private final double price = 225;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public double getPrice() {
        return price;
    }
}
