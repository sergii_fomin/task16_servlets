package com.fomin.model.impl;

import com.fomin.model.Pizza;

public class CheesePizza implements Pizza {
    private final String name = "Cheese pizza";

    private final double price = 230;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public double getPrice() {
        return price;
    }
}
