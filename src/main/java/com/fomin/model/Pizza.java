package com.fomin.model;

public interface Pizza {
    String getName();

    double getPrice();
}
